/*
 * msp_430_defines.h
 *
 *  Created on: Apr 16, 2018
 *      Author: Garrett Deguchi
 */

#ifndef HAL_MSP_430_DEFINES_H_
#define HAL_MSP_430_DEFINES_H_


/* These are extra defines for the MSP430G2553 */


/************************************************************
* BOOLEAN
************************************************************/
#define FALSE                   (0)
#define TRUE                    (1)

/************************************************************
* PORTS
************************************************************/
#define UNDEFINED_PORT          (0)
#define PORT1                   (1)
#define PORT2                   (2)

/************************************************************
* INPUT/OUTPUT
************************************************************/
#define OUTPUT                  (0)
#define INPUT                   (1)

/************************************************************
* OUTPUT
************************************************************/
#define LOW                     (0)
#define HIGH                    (1)
#define TOGGLE                  (2)

/************************************************************
* RESISTOR TYPE
************************************************************/
#define NOPULL                   (0)
#define PULLUP                   (1)
#define PULLDOWN                 (2)

#endif /* HAL_MSP_430_DEFINES_H_ */
