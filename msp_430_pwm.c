/*
 * msp_430_pwm.c
 *
 *  Created on: Apr 3, 2018
 *      Author: Garrett Deguchi
 */
#include <msp430.h>
#include <math.h>
#include "msp_430_defines.h"
#include "msp_430_gpio.h"
#include "msp_430_pwm.h"

#define MAX_PIN_COUNT 8
#define UNDEFINED_PIN 100
#define UNDEFINED_DUTY_CYCLE 101


static unsigned int pins[MAX_PIN_COUNT] =
{
    BIT0,
    BIT1,
    BIT2,
    BIT3,
    BIT4,
    BIT5,
    BIT6,
    BIT7,
    BIT8
};

typedef struct
{
    unsigned int port;
    unsigned int duty_cycle;
    unsigned int pin;
    unsigned int flag_high;
    unsigned int flag_low;
} pwm_id;

static pwm_id pwm_placement[MAX_PIN_COUNT]; // Max PWM count
static unsigned int active_pwm_pins = 0U;

/* Initializes the pin and port to be a PWM output NOTE: THE LAST PERIOD WILL BE PERIOD FOR ALL PWMs :: PWM USES TIMER0A0
 * pin: {BIT0 | BIT1 | ... | BITX}
 * port: {PORT1 | PORT2}
 * period: {put period in us for timer}
 * duty_cycle: {0 - 100}
 * */
void Initialize_PWM_Module(unsigned int pin, unsigned int port, unsigned int period, unsigned int duty_cycle)
{
    static unsigned int i = 0;

    if(port == PORT1)                               // Port 1 can only use timers on TA0
    {
        Set_GPIO_Pin(pin, OUTPUT, port, NOPULL);    // Turns on the specific pin
        Output_GPIO_Pin(pin, port, LOW);

        for(i = 0; i < MAX_PIN_COUNT; i++)
        {
            if(pin & pins[i])
            {
                pwm_placement[active_pwm_pins].port = PORT1;
                pwm_placement[active_pwm_pins].duty_cycle = duty_cycle;
                pwm_placement[active_pwm_pins].pin = pins[i];
                pwm_placement[active_pwm_pins].flag_high = TRUE;
                pwm_placement[active_pwm_pins].flag_low = TRUE;
                active_pwm_pins++;
            }
        }

        TA0CCTL0  = CCIE;                           // Reset/set
        TA0CCR0   = period;                         // Period on comparator 0
        TA0CTL    = (TASSEL_2 | MC_1 );             // SMCLK, timer in up-mode

    }
    else if(port == PORT2)          // Port 2 can only use timers on TA1
    {
        Set_GPIO_Pin(pin, OUTPUT, port, NOPULL);    // Turns on the specific pin
        Output_GPIO_Pin(pin, port, LOW);

        for(i = 0; i < MAX_PIN_COUNT; i++)
        {
            if(pin & pins[i])
            {
                pwm_placement[active_pwm_pins].port = PORT2;
                pwm_placement[active_pwm_pins].duty_cycle = duty_cycle;
                pwm_placement[active_pwm_pins].pin = pins[i];
                pwm_placement[active_pwm_pins].flag_high = TRUE;
                pwm_placement[active_pwm_pins].flag_low = TRUE;
                active_pwm_pins++;
            }
        }

        TA0CCTL0  = CCIE;                           // Reset/set
        TA0CCR0   = period;                         // Period on comparator 0
        TA0CTL    = (TASSEL_2 | MC_1 );             // SMCLK, timer in up-mode
    }
    else
    {
        // SHOULD NEVER REACH HERE
    }
}

/* Sets the PWM duty cycle on specified pin and port
 * pin: {BIT0 | BIT1 | ... | BITX}
 * port: {PORT1 | PORT2}
 * duty_cycle: {0 - 100}
 * */
void Set_PWM_Duty_Cycle(unsigned int pin, unsigned int port, unsigned int duty_cycle)
{
    static unsigned int i = 0;

    for(i = 0; i < active_pwm_pins; i++)
    {
        if((pin & pwm_placement[i].pin) && (port & pwm_placement[i].port))
        {
            pwm_placement[i].duty_cycle = duty_cycle;
        }
    }
}

static unsigned int counter = 0;

#pragma vector=TIMER0_A0_VECTOR
__interrupt void Timer0_A0_ISR( void )// TA0CCR0
{
    static unsigned int i;

    for(i = 0; i < active_pwm_pins; i++)
    {
        if(counter < pwm_placement[i].duty_cycle)
        {
            if(pwm_placement[i].flag_high == TRUE)
            {
                Output_GPIO_Pin(pwm_placement[i].pin, pwm_placement[i].port, HIGH);
                pwm_placement[i].flag_high = FALSE;
                pwm_placement[i].flag_low = TRUE;
            }
        }
        else
        {
            if(pwm_placement[i].flag_low == TRUE)
            {
                Output_GPIO_Pin(pwm_placement[i].pin, pwm_placement[i].port, LOW);
                pwm_placement[i].flag_high = TRUE;
                pwm_placement[i].flag_low = LOW;
            }
        }
    }

    counter++;
    if(counter > 100)
    {
        counter = 0;
    }
}
