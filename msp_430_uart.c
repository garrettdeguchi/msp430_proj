/*
 * msp_430_uart.c
 *
 *  Created on: Apr 24, 2018
 *      Author: gdeguchi
 */
#include <msp430.h>
#include "msp_430_defines.h"
#include "msp_430_uart.h"

/* Initializes the UART on pins P1.1 and P1.2 which are the only available ones on the msp430 */
void Initialize_UART(void)
{
    P1SEL |= BIT1 + BIT2;    // RXD
    P1SEL2 |= BIT1 + BIT2;   // TXD

    UCA0CTL1 |= UCSSEL_2 + UCSWRST;    // Use the System Master Clock at 1MHz

    // Look at page 424 of TRM to see commonly used baud rates
    UCA0BR0 = 104;                  // 1MHz 9600
    UCA0BR1 = 0;                    // 1MHz 9600
    UCA0MCTL = UCBRS_1;             // Modulation pattern
    UCA0CTL1 &= ~UCSWRST;           // Start USCI state machine

    //   IE2 |= UCA0TXIE;                  // Enable the Transmit interrupt
    IE2 |= UCA0RXIE;                  // Enable the Receive  interrupt
}

/* Sends the data over the UART
 * pin: {any string/character array}
 * length: {the length of the data being sent over}
 * */
void Transmit_UART(unsigned char * data, unsigned int length)
{
    static unsigned int i = 0;
    while(i < length)
    {
        while((UCA0STAT & UCBUSY));// Sit until the line is not busy with data
        UCA0TXBUF = data[i];
        i++;
    }
    i = 0;
}

#pragma vector = USCIAB0TX_VECTOR
__interrupt void TransmitInterrupt(void)
{

}

#define UART_BUFFER_SIZE 100

static unsigned char uart_buffer[UART_BUFFER_SIZE];
static unsigned int uart_buffer_counter = 0;

#pragma vector = USCIAB0RX_VECTOR
__interrupt void ReceiveInterrupt(void)
{
    unsigned char read_data;

    read_data = UCA0RXBUF;

    IFG2 &= ~UCA0RXIFG; // Clear RX flag

    if(read_data == '\r')
    {
        Transmit_UART(&uart_buffer, uart_buffer_counter);
        Transmit_UART("\n\r", 2);
        uart_buffer_counter = 0;
    }
    else
    {
        uart_buffer[uart_buffer_counter] = read_data;
        uart_buffer_counter++;
    }

}
