/*
 * msp_430_pwm.h
 *
 *  Created on: Apr 3, 2018
 *      Author: Garrett Deguchi
 */

#ifndef HAL_MSP_430_PWM_H_
#define HAL_MSP_430_PWM_H_

/* Initializes the pin and port to be a PWM output
 * pin: {BIT0 | BIT1 | ... | BITX}
 * port: {PORT1 | PORT2}
 * period: {put period in us for timer}
 * duty_cycle: {0 - 100}
 * */
void Initialize_PWM_Module(unsigned int pin, unsigned int port, unsigned int period, unsigned int duty_cycle);

/* Sets the PWM duty cycle on specified pin and port
 * pin: {BIT0 | BIT1 | ... | BITX}
 * port: {PORT1 | PORT2}
 * duty_cycle: {0 - 100}
 * */
void Set_PWM_Duty_Cycle(unsigned int pin, unsigned int port, unsigned int duty_cycle);

#endif /* HAL_MSP_430_PWM_H_ */
