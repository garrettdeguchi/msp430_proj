/*
 * msp_430_gpio.h
 *
 *  Created on: Apr 3, 2018
 *      Author: Garrett Deguchi
 */

#ifndef MSP_430_GPIO_H_
#define MSP_430_GPIO_H_

/* Initializes all the pins to on the MSP430 to be output pins driven low */
void Initialize_GPIO_Pins(void);

/* Initializes the GPIO pin/s specified
 * pin: {BIT0 | BIT1 | ... | BITX}
 * direction: {INPUT | OUTPUT}
 * port: {PORT1 | PORT2}
 * */
void Set_GPIO_Pin(unsigned int pin, unsigned int direction, unsigned int port, unsigned int resistor_type);

/* Sets the output of the GPIO pin/s specified
 * pin: {BIT0 | BIT1 | ... | BITX}
 * port: {PORT1 | PORT2}
 * output: {HIGH | LOW}
 * */
void Output_GPIO_Pin(unsigned int pin, unsigned int port, unsigned int output);

/* Checks the status of the pin whether it is high or low
 * pin: {BIT0 | BIT1 | ... | BITX}
 * port: {PORT1 | PORT2}
 * */
unsigned int Input_GPIO_Pin(unsigned int pin, unsigned int port);

#endif /* MSP_430_GPIO_H_ */
