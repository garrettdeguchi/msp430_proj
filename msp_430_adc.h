/*
 * msp_430_adc.h
 *
 *  Created on: Apr 16, 2018
 *      Author: Garrett Deguchi
 */

#ifndef HAL_MSP_430_ADC_H_
#define HAL_MSP_430_ADC_H_

/* Initializes the pin to be an ADC pin
 * pin: {BIT0 | BIT1 | ... | BITX}
 * port: {PORT1 | PORT2}
 * */
void Initialize_ADC_Pin(unsigned int pin, unsigned int port);

/* Read back from the ADC pin
 * pin: {BIT0 | BIT1 | ... | BITX}
 * port: {PORT1 | PORT2}
 * */
unsigned int Read_ADC_Pin(unsigned int pin, unsigned int port);

#endif /* HAL_MSP_430_ADC_H_ */
