/*
 * msp_430_gpio.c
 *
 *  Created on: Apr 3, 2018
 *      Author: Garrett Deguchi
 */

#include <msp430.h>
#include "msp_430_defines.h"
#include "msp_430_gpio.h"

/* Initializes all the pins to on the MSP430 to be output pins driven low */
void Initialize_GPIO_Pins(void)
{

    P1DIR = (BIT0 | BIT1 | BIT2 | BIT3 | BIT4 | BIT5 | BIT6 | BIT7);    // Set all the directions of the pins to be outputs for port 1
    P2DIR = (BIT0 | BIT1 | BIT2 | BIT3 | BIT4 | BIT5);                  // Set all the directions of the pins to be outputs for port 2
    P1OUT = 0x00;                                                       // Sets all the outputs from port 1 to be driven low
    P2OUT = 0x00;                                                       // Sets all the outputs from port 2 to be driven low

}

/* Initializes the GPIO pin/s specified
 * pin: {BIT0 | BIT1 | ... | BITX}
 * direction: {INPUT | OUTPUT}
 * port: {PORT1 | PORT2}
 * */
void Set_GPIO_Pin(unsigned int pin, unsigned int direction, unsigned int port, unsigned int resistor_type)
{

    if(direction == INPUT)
    {
        if(port == PORT1)
        {
            P1DIR &= ~pin;  //Set the pin direction to be an input for port 1
            P1SEL &= ~pin;  //Makes sure that the pin is a typical GPIO for port 1

            if(resistor_type != NOPULL)
            {
                P1REN |= pin;   //Enable the pull up/down resistor for port 1
                if(resistor_type == PULLUP)
                {
                    P1OUT |= pin;   //Pull up resistor pin for port 1
                }
                else
                {
                    P1OUT &= ~pin;  //Pull down resistor pin for port 1
                }
            }

//            P1IE &= pin;    // Enables interrupt for the specified pin //Garrett
//            P1IFG &= ~pin;  // Clears the interrupt flag for the pin

        }
        else if(port == PORT2)
        {
            P2DIR &= ~pin;  //Set the pin direction to be an input for port 2
            P2SEL &= ~pin;  //Makes sure that the pin is a typical GPIO for port 2

            if(resistor_type != NOPULL)
            {
                P2REN |= pin;   //Enable the pull up/down resistor for port 2
                if(resistor_type == PULLUP)
                {
                    P2OUT |= pin;   //Pull up resistor pin for port 2
                }
                else
                {
                    P2OUT &= ~pin;  //Pull down resistor pin for port 2
                }
            }

//            P2IE &= pin;    // Enables interrupt for the specified pin //Garrett
//            P2IFG &= ~pin;  // Clears the interrupt flag for the pin

        }
        else
        {
            // SHOULD NEVER REACH HERE
        }
    }
    else if(direction == OUTPUT)
    {
        if(port == PORT1)
        {
            P1DIR |= pin;   // Set the pin direction to be an output pin on port 1
            P1SEL &= ~pin;  //Makes sure that the pin is a typical GPIO for port 1
        }
        else if(port == PORT2)
        {
            P2DIR |= pin;   // Set the pin direction to be an output pin on port 2
            P2SEL &= ~pin;  //Makes sure that the pin is a typical GPIO for port 2
        }
        else
        {
            // SHOULD NEVER REACH HERE
        }
    }
    else
    {
        // SHOULD NEVER REACH HERE
    }

}

/* Sets the output of the GPIO pin/s specified
 * pin: {BIT0 | BIT1 | ... | BITX}
 * port: {PORT1 | PORT2}
 * output: {HIGH | LOW | TOGGLE}
 * */
void Output_GPIO_Pin(unsigned int pin, unsigned int port, unsigned int output)
{

    if(port == PORT1)
    {
        if(output == HIGH)          // Set the output to be high
        {
            P1OUT |= pin;           // Set the output pin to be high on the pin for port 1
        }
        else if(output == TOGGLE)   // Toggle the output
        {
            P1OUT ^= pin;
        }
        else                        // Set the output to be low
        {
            P1OUT &= ~pin;          // Set the output pin to be low on the pin for port 1
        }
    }
    else if(port == PORT2)
    {
        if(output == HIGH)          // Set the output to be high
        {
            P2OUT |= pin;           // Set the output pin to be high on the pin for port 2
        }
        else if(output == TOGGLE)   // Toggle the output
        {
            P1OUT ^= pin;
        }
        else                        // Set the output to be low
        {
            P2OUT &= ~pin;          // Set the output pin to be low on the pin for port 2
        }
    }
    else
    {
        // SHOULD NEVER REACH HERE
    }

}

/* Checks the status of the pin whether it is high or low
 * pin: {BIT0 | BIT1 | ... | BITX}
 * port: {PORT1 | PORT2}
 * */
unsigned int Input_GPIO_Pin(unsigned int pin, unsigned int port)
{

    unsigned int return_value = FALSE;  // Returns the status of the pin

    if(port == PORT1)   // Checks for port 1 pin
    {
        if((P1IN & pin) == TRUE)
        {
            return_value = TRUE;    // If the pin checks out then it returns TRUE
        }
        else
        {
            return_value = FALSE;   // If the pin does not check out then it returns FALSE
        }
    }
    else if(port == PORT2)  // Checks for port 2 pin
    {
        if((P2IN & pin) == TRUE)
        {
            return_value = TRUE;    // If the pin checks out then it returns TRUE
        }
        else
        {
            return_value = FALSE;   // If the pin does not check out then it returns FALSE
        }
    }
    else
    {
        // SHOULD NEVER REACH HERE
    }

    return return_value;

}
