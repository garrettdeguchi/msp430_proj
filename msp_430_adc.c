/*
 * msp_430_adc.c
 *
 *  Created on: Apr 16, 2018
 *      Author: Garrett Deguchi
 */

#include <msp430.h>
#include "msp_430_defines.h"
#include "msp_430_adc.h"

/* Initializes the pin to be an ADC pin
 * pin: {BIT0 | BIT1 | ... | BITX}
 * port: {PORT1 | PORT2}
 * */
void Initialize_ADC_Pin(unsigned int pin, unsigned int port)
{
    if(port == PORT1)
    {
        P1SEL |= pin;

        switch(pin)
        {
            case BIT0:
                ADC10CTL1 = INCH_0 + ADC10DIV_3;
                break;
            case BIT1:
                ADC10CTL1 = INCH_1 + ADC10DIV_3;
                break;
            case BIT2:
                ADC10CTL1 = INCH_2 + ADC10DIV_3;
                break;
            case BIT3:
                ADC10CTL1 = INCH_3 + ADC10DIV_3;
                break;
            case BIT4:
                ADC10CTL1 = INCH_4 + ADC10DIV_3;
                break;
            case BIT5:
                ADC10CTL1 = INCH_5 + ADC10DIV_3;
                break;
            case BIT6:
                ADC10CTL1 = INCH_6 + ADC10DIV_3;
                break;
            case BIT7:
                ADC10CTL1 = INCH_7 + ADC10DIV_3;
                break;
            case BIT8:
                ADC10CTL1 = INCH_8 + ADC10DIV_3;
                break;
            default:
                break;

        }
        ADC10CTL0 = SREF_0 + ADC10SHT_3 + ADC10ON + ADC10IE; //Vcc & Vss as reference
        ADC10AE0 |= pin; //P1.5 ADC option
    }
    else if(port == PORT2)// ADC do not have any on PORT 2
    {

    }
    else
    {
        // SHOULD NEVER REACH HERE
    }
}

/* Read back from the ADC pin
 * pin: {BIT0 | BIT1 | ... | BITX}
 * port: {PORT1 | PORT2}
 * return: returns percentage of ADC read back
 * */
unsigned int Read_ADC_Pin(unsigned int pin, unsigned int port)
{
    static unsigned int value = 0;

    ADC10CTL0 |= ENC + ADC10SC; // Sampling and conversion start
    value = ADC10MEM;// Grabs sample from memory
    value = ((value * 100) / 1024);

    return value;
}

//Interrupt vector for the ADC
#pragma vector=ADC10_VECTOR
__interrupt void ADC10_ISR (void)
{
   __bic_SR_register_on_exit(CPUOFF); // Return to active mode

}
