/*
 * msp_430_uart.h
 *
 *  Created on: Apr 24, 2018
 *      Author: Garrett Deguchi
 */

#ifndef HAL_MSP_430_UART_H_
#define HAL_MSP_430_UART_H_

/* Initializes the UART on pins P1.1 and P1.2 which are the only available ones on the msp430 */
void Initialize_UART(void);

/* Sends the data over the UART
 * pin: {any string/character array}
 * length: {the length of the data being sent over}
 * */
void Transmit_UART(unsigned char * data, unsigned int length);

#endif /* HAL_MSP_430_UART_H_ */
